//
//  DateHistoryDetailViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/14/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class DateHistoryDetailViewController: UIViewController {

    
    @IBOutlet var date: UILabel!
    @IBOutlet var time: UILabel!
    @IBOutlet var attire: UILabel!
    @IBOutlet var location: UILabel!
    @IBOutlet var sender: UILabel!
    var theDate:String!
    var theTime:String!
    var theAttire:String!
    var theLocation:String!
    var theSender:String!
    var spouseRelation:PFRelation = PFRelation()
    var recipient:NSMutableArray = []
    var spouse:NSArray = []
    var currentUser:PFUser = PFUser()
    var counter = 0
    var someArray:NSArray = []
    var theIndex:Int!
    var random:Int?
    var user:PFUser!
    var finalRandomNumber:Int!
    var recName:String?
    var receiverName:String?
    @IBOutlet var response: UITextField!
    @IBOutlet var acceptButton: UIButton!
    
    var newRandom:Int?
    var rRandom:Int?
    var theConfirmedNumber:Int?
    var theRandomNumber:Int?
    var theRecName:String?
 
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //acceptButton.enabled = false
        //acceptButton.tintColor = UIColor.grayColor()
        
        self.hideKeyboardWhenTappedAround()
        
         spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
        
        if let nav = navigationController?.navigationBar{
          
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.grayColor()]
            
        }
        
        let query = spouseRelation.query()
        query.orderByAscending("username")
        query.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else {
                self.spouse = objects!
                
            }
            
            self.user = self.spouse.objectAtIndex(0) as! PFUser
            print(self.user.username)
            
            let aQuery = PFQuery(className: "DateRequest")
            aQuery.whereKey("finalStatus", containsString: "accepted")
            aQuery.whereKey("recUsername", containsString: PFUser.currentUser()?.username)
            //aQuery.whereKey("random", containsString: "confirmedNumber")
            aQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) in
                
                if objects != nil{
                    print(objects)
                    
                    for object in objects!{
                        
                        let bQuery = PFQuery(className: "DateRequest")
                        
                        let num:Int? = object.objectForKey("confirmedNumber") as? Int
                         self.recName = object.objectForKey("recUsername") as? String
                        self.random = object.objectForKey("randomNumber") as? Int
                        let finalStatus = object.objectForKey("finalStatus") as? String
                        if num == nil{
                            print("nil confirmed number")
                        } else if let myConfirmedNum = num{
                            print(myConfirmedNum)
                            self.theConfirmedNumber = myConfirmedNum
                           
                          
                        } else {
                            print("no num")
                        }
                       
                       
                        if self.random == nil{
                            print("nil random number")
                        } else if let myRandom = self.random{
                            print(myRandom)
                            self.theRandomNumber = myRandom
                        } else {
                            print("no numbers")
                        }
                        
                        if self.recName == nil{
                            print("nil name")
                        } else if let newRecName = self.recName {
                            print(newRecName)
                            self.theRecName = newRecName
                            
                        } else {
                            print("no name")
                        }
                         bQuery.whereKey("confirmedNumber", equalTo: self.theConfirmedNumber!)
                         bQuery.whereKey("recUsername", equalTo: self.theRecName!)
                        bQuery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?, error:NSError?) in
                            
                            if objects != nil {
                                
                                if objects! == []{
                                    print("array empty")
                                    
                                } else {
                                    for object in objects!{
                                        print(object)
                                        if self.theRecName == PFUser.currentUser()?.username && finalStatus == "accepted" && self.theRandomNumber == self.theConfirmedNumber{
                                            self.acceptButton.enabled = false
                                            self.acceptButton.backgroundColor = UIColor.grayColor()
                                        } else {
                                            print("enable")
                                        }
                                        
                                    }
                                }
                                
                            } else {
                                print(error?.localizedDescription)
                            }
                        })
                        //bQuery.whereKey("randomNumber", containsString: String?)
                        /*
                        if num == random && recName == PFUser.currentUser()?.username{
                            print("need to disable")
                            self.acceptButton.enabled = false
                            self.acceptButton.backgroundColor = UIColor.grayColor()
                        } else {
                            print("no random")
                            self.acceptButton.enabled = true
                            self.acceptButton.backgroundColor = UIColor.blueColor()
                        }
                        */
                    }
                    
                    if objects! == [] {
                        
                    } else {
                        
                    }
                } else {
                    print(error?.localizedDescription)
                }
 
            }

            //self.recipient.addObject(user.objectId!)
        })
        
        
        /*
       let acceptedDateQuery = PFQuery(className: "DateRequest")
        
        //acceptedDateQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        acceptedDateQuery.orderByDescending("createdAt")
        //acceptedDateQuery.whereKey("senderName", equalTo: (user)
        //acceptedDateQuery.whereKey("senderName", equalTo: user.username!)
       // acceptedDateQuery.whereKey("status", containsString: "accepted")
        //acceptedDateQuery.whereKey("status", notEqualTo: "accepted")
        acceptedDateQuery.whereKey("recUsername", containsString: PFUser.currentUser()?.username)
        acceptedDateQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
        
            for object in objects!{
                if let newRandom:Int? = object.objectForKey("acceptedRandom") as? Int{
                    
                    print(newRandom)
                    if newRandom == nil{
                        print("no numbers yet still nil")
                    } else if newRandom <= 1000{
                        
                            //enable button
                            self.acceptButton.enabled = true
                            self.acceptButton.tintColor = UIColor.blueColor()
                        } else {
                            //disable button
                            self.acceptButton.enabled = false
                            self.acceptButton.tintColor = UIColor.grayColor()
                        }

                
                    
                    
                } else {
                    print("no newRandom")
                    self.acceptButton.enabled = true
                    self.acceptButton.tintColor = UIColor.blueColor()
                }
                self.acceptButton.enabled = true
                self.acceptButton.tintColor = UIColor.blueColor()
                
            }
            print(objects)
           
            
        }
        */
        
        
           
        //put optional in to mae sure data is avaiable for date/time/attire/location
       print(theDate)
        
        date.text = theDate
        //time.text = theTime
        attire.text = theAttire
        location.text = theLocation
        
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
    }

    @IBAction func acceptedResponse(sender: AnyObject) {
        
   
        
        let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
        self.recipient.addObject(user.objectId!)
        let comments = response.text
        //counter = 1
        let dateRequest = PFObject(className:"DateRequest")
        //dateRequest["counter"] = counter
        dateRequest["status"] = "accepted"
        dateRequest["finalStatus"] = "accepted"
        dateRequest["dateComments"] = comments
        //dateRequest["acceptedRandom"] = random
        dateRequest["confirmedDate"] = theDate
        dateRequest["confirmedDressType"] = theAttire
        dateRequest["confirmedLocation"] = theLocation
        dateRequest["confirmedComments"] = comments
        dateRequest["confirmedNumber"] = self.random
        dateRequest["recUsername"] = self.recName
        dateRequest.setObject(recipient, forKey: "recipientsIds")
        dateRequest.setObject(PFUser.currentUser()!, forKey: "senderId")
        dateRequest.setObject((PFUser.currentUser()?.username)!, forKey: "senderName")
        
        dateRequest.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
            
            if success{
                
                
                let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                self.presentViewController(vc, animated: true, completion: nil)
            } else {
                print(error?.localizedDescription)
            }

            
        }

    }
    
    
    @IBAction func cancelButtonTapped(sender: AnyObject) {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.presentViewController(vc, animated: true, completion: nil)
    }
    


}
