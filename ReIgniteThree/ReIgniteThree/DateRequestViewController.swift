//
//  DateRequestViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/7/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse


class DateRequestViewController: UIViewController {

    @IBOutlet var dateField: UITextField!
    @IBOutlet var timeField: UITextField!
    @IBOutlet var placeField: UITextField!
    @IBOutlet var dressTypeField: UITextField!
    var spouseRelation:PFRelation = PFRelation()
    var recipient:NSMutableArray = []
    var spouse:NSArray = []
    var location:String!
    var date:String!
    var time:String!
    var dressType:String!
    var strDate:String!
    var myStrDate:String!
    var counter = 0
    var mySpouseArray:NSArray = []
    var acceptedArray:NSArray = []
    @IBOutlet var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
         self.hideKeyboardWhenTappedAround()
        
        if let nav = navigationController?.navigationBar{
           
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            
        }
        
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let fQuery:PFQuery = PFQuery(className: "SpouseReply")
        //fQuery.whereKey("other", containsString: PFUser.currentUser()?.username)
        fQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) in
            
            if objects != nil {
                
            }
            
        }
        
                        
     
                        
                        print("name doesnt match")
                        

                        let sQuery:PFQuery = PFQuery(className: "SpouseReply")
                        sQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
                        //sQuery.whereKey("recUser", containsString: PFUser.currentUser()?.username)
                        sQuery.orderByAscending("createdAt")
                        sQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
                            
                            if objects != nil {
                                
                                
                                self.mySpouseArray = objects!
                                print(self.mySpouseArray.count)
                                print(self.mySpouseArray)
                                
                                
                                
                                if self.mySpouseArray.count == 1 {
                                    //cool to enter initialize spouseRelation
                                    self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                                    let squery = self.spouseRelation.query()
                                    squery.orderByAscending("username")
                                    squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                                        
                                        if error != nil{
                                            print(error?.localizedDescription)
                                        } else {
                                            self.spouse = objects!
                                            
                                        }
                                        
                                        let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
                                        
                                        if user.objectId != nil{
                                            //have a user hide connection button
                                            
                                            
                                        } else {
                                            
                                        }
                                        
                                        self.recipient.addObject(user.objectId!)
                                    })
                                    
                                } else {
                                    //check if you have an accepted connection
                                    
                                    let aQuery:PFQuery = PFQuery(className: "AcceptedConnection")
                                    aQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
                                    aQuery.orderByAscending("createdAt")
                                    aQuery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?, error:NSError?) in
                                        
                                        if objects != nil {
                                            self.acceptedArray = objects!
                                            if self.acceptedArray.count >= 1{
                                                //yay they can do their thang
                                                
                                                self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                                                let squery = self.spouseRelation.query()
                                                squery.orderByAscending("username")
                                                squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                                                    
                                                    if error != nil{
                                                        print(error?.localizedDescription)
                                                    } else {
                                                        self.spouse = objects!
                                                        
                                                    }
                                                    
                                                    let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
                                                    
                                                    if user.objectId != nil{
                                                        //have a user hide connection button
                                                        
                                                        
                                                    } else {
                                                        
                                                    }
                                                    
                                                    self.recipient.addObject(user.objectId!)
                                                })
                                                
                                            } else {
                                                let alertController = UIAlertController(title: "Sorry! You have to wait till your baby connects", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                                                
                                                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                                                    
                                                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                                                    let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                                                    self.presentViewController(vc, animated: true, completion: nil)
                                                    
                                                })
                                                alertController.addAction(defaultAction)
                                                self.presentViewController(alertController, animated: true, completion: nil)
                                            }
                                        } else {
                                            print(error?.localizedDescription)
                                        }
                                    })
                                    
                                    
                                    
                                    
                                    /*
                                     let alertController = UIAlertController(title: "Sorry! You have to wait till your baby connects", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                                     
                                     let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                                     
                                     let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                                     let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                                     self.presentViewController(vc, animated: true, completion: nil)
                                     
                                     })
                                     alertController.addAction(defaultAction)
                                     self.presentViewController(alertController, animated: true, completion: nil)
                                     */
                                }
                                
                            } else {
                                print(error?.localizedDescription)
                            }
                        }

        
    }
    override func viewDidAppear(animated: Bool) {
        /*
        self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
        let squery = self.spouseRelation.query()
        squery.orderByAscending("username")
        squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else {
                self.spouse = objects!
                
            }
            
            let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
            
            if user.objectId != nil{
                //have a user hide connection button
                
                
            } else {
                
            }
            
            self.recipient.addObject(user.objectId!)
        })
        */

    }
    
    @IBAction func datePickerAction(sender: AnyObject) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm"
        strDate = dateFormatter.stringFromDate(datePicker.date)
        print(strDate)

    }

    
    func datePickerChanged(datePicker:UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        
        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
        strDate = dateFormatter.stringFromDate(datePicker.date)
        
        //dateLabel.text = strDate
    }
    


    func sendDateRequest(){
        
        location = placeField.text
        dressType = dressTypeField.text
       
        //print(strDate)
       
        if location.characters.count <= 4 && dressType.characters.count <= 3{
            
            let alertController = UIAlertController(title: "Not enough characters entered", message: "Please re-enter", preferredStyle: UIAlertControllerStyle.Alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                
            })
            alertController.addAction(defaultAction)
            self.presentViewController(alertController, animated: true, completion: nil)

            
        } else {
            
      
            let dateRequest = PFObject(className:"DateRequest")
            var random = Int(arc4random_uniform(53))
             print("first random:\(random)")
            random = random * 12 + 1 * 32 - 5 * 3
            print(random)
            //let myRandom = random
            /*
            if random == myRandom {
                 random = random + 2 * 9 * 32 - 1 + 4 * 6
                print(random)
            }
            */
            print("second random:\(random)")
            dateRequest["location"] = location
            //dateRequest["date"] = date
            //dateRequest["time"] = time
            let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
            //self.recipient.addObject(user.objectId!)
            print(user.username)
            counter = 1
            if let myDate = strDate {
                
                print(myDate)
                dateRequest["datePicker"] = myDate
            } else {
                print("no valid date")
            }
            //dateRequest["counter"] = counter
            dateRequest["dressType"] = dressType
            dateRequest["recUsername"] = user.username
            //dateRequest["random"] = random
            dateRequest.setObject(recipient, forKey: "recipientsIds")
            dateRequest.setObject(random, forKey: "randomNumber")
            dateRequest.setObject(PFUser.currentUser()!, forKey: "senderId")
            dateRequest.setObject((PFUser.currentUser()?.username)!, forKey: "senderName")
            
            dateRequest.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
                
                if success{
                    
                    
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                    self.presentViewController(vc, animated: true, completion: nil)

                    
                    
                } else {
                    print(error?.localizedDescription)
                }
            }

            
        }
        /*
        if location.characters.count <= 2 && date.characters.count < 4 && time.characters.count < 2 {
            
            //alert not enough data
            
            let alertController = UIAlertController(title: "Not enough characters entered", message: "Please re-enter", preferredStyle: UIAlertControllerStyle.Alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                
            })
            alertController.addAction(defaultAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            
        } else {
            
            let dateRequest = PFObject(className:"DateRequest")
            dateRequest["location"] = location
            dateRequest["date"] = date
            dateRequest["time"] = time
            dateRequest["datePicker"] = strDate
            dateRequest["dressType"] = dressType
            dateRequest.setObject(recipient, forKey: "recipientsIds")
            dateRequest.setObject(PFUser.currentUser()!, forKey: "senderId")
            dateRequest.setObject((PFUser.currentUser()?.username)!, forKey: "senderName")
            
            dateRequest.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
                
                if success{
                    
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                    self.presentViewController(vc, animated: true, completion: nil)
                    
                    
                } else {
                    print(error?.localizedDescription)
                }
            }

        }
        */
        
    }
    @IBAction func askOutButtonTapped(sender: AnyObject) {
        sendDateRequest()
    }
    
   
    
    @IBAction func cancelButtonTapped(sender: AnyObject) {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
}
