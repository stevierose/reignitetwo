//
//  PartnersAcceptTableViewCell.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/30/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit

class PartnersAcceptTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var dressType: UILabel!
    @IBOutlet weak var comments: UILabel!
    
    
    
}
