//
//  DateHistoryViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/13/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class DateHistoryViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var date:NSArray = []
    var nDate:String!
    var nTime:String!
    var nLocation:String!
    var nAttire:String!
    var myDate:String!
    var spouseRelation:PFRelation = PFRelation()
    var spouseObjects:NSArray = []
    var mySpouseArray:NSArray = []
    var spouse:NSArray = []
    var acceptedArray:NSArray = []
    var random:Int!
    
    
    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let nav = navigationController?.navigationBar{
            
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.grayColor()]
            
            
        }
        let sQuery:PFQuery = PFQuery(className: "SpouseReply")
        sQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        sQuery.orderByAscending("createdAt")
        sQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if objects != nil {
                
                self.mySpouseArray = objects!
                print(self.mySpouseArray.count)
                print(self.mySpouseArray)
                
                if self.mySpouseArray.count == 1 {
                    //cool to enter initialize spouseRelation
                    self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                    let squery = self.spouseRelation.query()
                    squery.orderByAscending("username")
                    squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                        
                        if error != nil{
                            print(error?.localizedDescription)
                        } else {
                            self.spouse = objects!
                            
                        }
                        
                        let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
                        
                        if user.objectId != nil{
                            //have a user hide connection button
                            
                            
                        } else {
                            
                        }
                        
                        //self.recipient.addObject(user.objectId!)
                    })
                    
                } else {
                    //check if you have an accepted connection
                    
                    let aQuery:PFQuery = PFQuery(className: "AcceptedConnection")
                    aQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
                    aQuery.orderByAscending("createdAt")
                    aQuery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?, error:NSError?) in
                        
                        if objects != nil {
                            self.acceptedArray = objects!
                            if self.acceptedArray.count == 1{
                                //yay they can do their thang
                                
                                self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                                let squery = self.spouseRelation.query()
                                squery.orderByAscending("username")
                                squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                                    
                                    if error != nil{
                                        print(error?.localizedDescription)
                                    } else {
                                        self.spouse = objects!
                                        
                                    }
                                    
                                    let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
                                    
                                    if user.objectId != nil{
                                        //have a user hide connection button
                                        
                                        
                                    } else {
                                        
                                    }
                                    
                                    //self.recipient.addObject(user.objectId!)
                                })
                                
                            } else {
                                let alertController = UIAlertController(title: "Sorry! You have to wait till your baby connects", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                                
                                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                                    
                                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                                    let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                                    self.presentViewController(vc, animated: true, completion: nil)
                                    
                                })
                                alertController.addAction(defaultAction)
                                self.presentViewController(alertController, animated: true, completion: nil)
                            }
                        } else {
                            print(error?.localizedDescription)
                        }
                    })
                    

        
                }
            }
        }
        
        
        
        

        // Do any additional setup after loading the view.
        
        
        let dateQuery:PFQuery = PFQuery(className:"DateRequest")
        dateQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        dateQuery.whereKey("status", notEqualTo: "accepted")
        dateQuery.orderByDescending("createdAt")
        dateQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else{
                //we have a message
                self.date = objects!
                print(self.date)
                print(self.date.count)
                self.tableView.reloadData()
                //let dates:PFObject = self.date.objectAtIndex(0) as! PFObject
            }
        }
        
        
        /*
        let sQuery:PFQuery = PFQuery(className: "SpouseReply")
        sQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        sQuery.orderByAscending("createdAt")
        sQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if objects != nil {
                self.spouseObjects = objects!
                
                    
            } else {
                print(error?.localizedDescription)
            }
        
        }
        */

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
   
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:DateHistoryTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! DateHistoryTableViewCell
        
        
        
        print(self.date.count)
        let dates:PFObject = date.objectAtIndex(indexPath.row) as! PFObject
        nLocation = dates.objectForKey("location") as? String
        //nTime = dates.objectForKey("time") as? String
        nDate = dates.objectForKey("datePicker") as? String
        nAttire = dates.objectForKey("dressType") as? String
        
     
        if let newLocation = nLocation {
            let myLocation = newLocation
            print(myLocation)
        }
        if let newDate = nDate {
              myDate = newDate
            print(myDate)
        }
        if let newAttire = nAttire{
            let myAttire = newAttire
            print(myAttire)
            
           
        } else {
            print("we have nothing in the request")
            
            //cell.timeLabel.hidden = true
            cell.placeLabel.hidden = true
            cell.dateLabel.hidden = true
            cell.attireLabel.hidden = true
            cell.location.hidden = true
            //cell.time.hidden = true
            cell.date.hidden = true
            cell.attire.hidden = true
        }
      
        
        cell.location?.text =  nLocation
        //cell.time?.text = nTime
        cell.date?.text = nDate
        cell.attire?.text = nAttire

        
        
        
        

        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("index:\(indexPath.row)")
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        let dates:PFObject = date.objectAtIndex(indexPath.row) as! PFObject
        nDate = dates.objectForKey("datePicker") as? String
        nLocation = dates.objectForKey("location") as? String
        nAttire = dates.objectForKey("dressType") as? String
  
        let myCount:String? = nDate
        print(myCount)
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("DateHistoryDetailViewController") as! DateHistoryDetailViewController
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                
        if let myDate = nDate{
            print(myDate)
            controller.theIndex = random
            controller.theDate = myDate
            controller.theLocation = nLocation
            controller.theAttire = nAttire
            appDelegate.window?.rootViewController = controllerNav
        } else {
            print("no date")
        }
       
        //controller.theTime = nTime
        
        
        
        
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return date.count
    }
    
    @IBAction func confirmedDateButtonTapped(sender: AnyObject) {
        
        
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("AcceptedDateRequestViewController") as! AcceptedDateRequestViewController
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window?.rootViewController = controllerNav

        
     

    }
    
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.presentViewController(vc, animated: true, completion: nil)
    }
   
}
