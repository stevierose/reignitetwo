//
//  TakeMeOutViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/13/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class TakeMeOutViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate{
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var tableView: UITableView!
    var placeId:String!
    let APIKEY = "AIzaSyCU0ybMB-w_7teJ-ijF4TBkPW4KY--CBUI"
    var placeIdUrlString:String!
    var locationManager = CLLocationManager()
    var places: [Place] = []
    var myPlaces:NSMutableArray = []
    var thePlaces:AnyObject = []
    var webObjects:NSArray = []
    var newLat:String!
    var newLng:String!
    var newName:String!
    var newAddress:String!
    var newRating:String!
    var newNumber:String!
    var placesArray:[Place] = []
    
    
    var didFindMyLocation = false
    
    var lat:CLLocationDegrees!
    var lng:CLLocationDegrees!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

        
        activityIndicator.hidden = false
        activityIndicator.startAnimating()
        
        if let nav = navigationController?.navigationBar{
            
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            
        }
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        //lat = locationManager.location?.coordinate.latitude
        //lng = locationManager.location?.coordinate.longitude
        
        
        //let myLat = String(lat)
        //let myLng = String(lng)
        
        let tempLat = 35.2270869
        let tempLng = -80.8431267
        let strTempLat = String(tempLat)
        let strTempLng = String(tempLng)
        //print(myLat)
        
        
        
        let urlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + strTempLat + "," + strTempLng + "&radius=2000&types=restaurants&key=AIzaSyCU0ybMB-w_7teJ-ijF4TBkPW4KY--CBUI"
        
        if let url = NSURL(string: urlString) {
            load(url)
        }
    }
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    
    func load(URL: NSURL) {
        let sessionConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let request = NSMutableURLRequest(URL: URL)
        request.HTTPMethod = "GET"
        let task = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) -> Void in
            if (error == nil) {
                // Success
                let statusCode = (response as! NSHTTPURLResponse).statusCode
                print("Success: \(statusCode)")
                
                do {
                    let jsonDict = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0)) as? NSDictionary
                    if let jsonDict = jsonDict {
                        
                        
                        if let results = jsonDict["results"] as? [[String:AnyObject]] {
                            print(results)
                            for result in results{
                                
                                self.placeId = result["place_id"] as? String
                                
                                self.placeIdUrlString = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + self.placeId + "&key=" + self.APIKEY + ""
                                
                                if let url = NSURL(string: self.placeIdUrlString) {
                                    self.loadPlaceId(url)
                                }
                                
                            }
                        }
                        
                    } else {
                        // more error handling
                        print("error that I'm not sure of")
                    }
                } catch let error as NSError {
                    // error handling
                    print(error.localizedDescription)
                }
            }
            else {
                // Failure
                print("Faulure: %@", error!.localizedDescription);
            }
            
            
        }
        task.resume()
        /*
         let task = session.dataTaskWithRequest(request, completionHandler: { (data: NSData!, response: NSURLResponse!, error: NSError!) -> Void in
         if (error == nil) {
         // Success
         let statusCode = (response as! NSHTTPURLResponse).statusCode
         print("Success: \(statusCode)")
         
         // This is your file-variable:
         // data
         }
         else {
         // Failure
         print("Faulure: %@", error.localizedDescription);
         }
         })
         task.resume()
         */
    }
    
    func loadPlaceId(url:NSURL) {
        
        
        let sessionConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        let task = session.dataTaskWithRequest(request) { (data:NSData?, response:NSURLResponse?, error:NSError?) in
            
            if error == nil {
                
                do {
                    let jsonDict = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
                    
                    if let results = jsonDict!["result"]{
                        
                        let place = Place()
                        print(results)
                        
                        if let address = results.objectForKey("formatted_address") {
                            //print(address)
                            place.address = address as! String
                        } else {
                            print("no address")
                        }
                        
                        if let number = results.objectForKey("formatted_phone_number") {
                            
                            //print(number)
                            place.phoneNumber = number as! String
                            
                        } else {
                            print("no number")
                        }
                        
                        if let rating = results.objectForKey("ratings") {
                            
                            print(rating)
                        } else {
                            print("no ratings")
                        }
                        
                        if let name = results.objectForKey("name") {
                            print(name)
                            place.name = name as! String
                        } else {
                            print("no name")
                        }
                        
                        if let myGeo = results.objectForKey("geometry")?.objectForKey("location"){
                            print(myGeo)
                            let lat = myGeo.objectForKey("lat")
                            let lng = myGeo.objectForKey("lng")
                            place.lat = ((lat)?.doubleValue)!
                            place.lng = ((lng)?.doubleValue)!
                            print(lat)
                            print(lng)
                            
                        } else {
                            print("no lat and lng")
                        }
                        
                        self.placesArray.append(place)
                        self.tableView.reloadData()
                        
                    }
                    
                } catch let error as NSError {
                    
                    print(error)
                }
                
            } else {
                
                
                
            }
            
            
        }
        task.resume()
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! TakeMeOutTableViewCell
        activityIndicator.stopAnimating()
        activityIndicator.hidden = true
       
        let thePlaces = placesArray[indexPath.row]
        
        cell.name.text = thePlaces.name
        cell.address.text = thePlaces.address
        cell.number.text = thePlaces.phoneNumber
        

        
       
       
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    
        let thePlaces = placesArray[indexPath.row]
        print(placesArray.count)
        
        
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("MapViewController") as! MapViewController
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

        controller.name = thePlaces.name
        controller.address = thePlaces.address
        controller.number = thePlaces.phoneNumber
 
        controller.lat = String(thePlaces.lat)
        controller.lng = String(thePlaces.lng)
        

        
        appDelegate.window?.rootViewController = controllerNav
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return placesArray.count
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.presentViewController(vc, animated: true, completion: nil)
        
        
    }
    
}


