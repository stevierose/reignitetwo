//
//  SearchViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/18/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse



class SearchViewController: UIViewController {
    
    var onlyUser:PFObject!
    var onlyUsers:NSArray = []
    var spouseRelation:PFRelation = PFRelation()
    var currentUser = PFUser()
    var spouseConnectionReply:PFObject!
    var mySomethingArray:NSArray = []
    var email:String!
    var recipeint:NSMutableArray = []
    var spouse:NSArray = []
    var requestedUser:NSArray = []
    var otherSpouseRelation:PFRelation = PFRelation()
    var user:PFUser = PFUser()
    var cUser:String!
     var spouseRecipient:NSMutableArray = []
    
    
    @IBOutlet weak var findBabyButton: UIButton!

    @IBOutlet var usernameField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let nav = navigationController?.navigationBar{
            
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            
        }
        
        findBabyButton.layer.cornerRadius = 5
        findBabyButton.layer.borderWidth = 1
        findBabyButton.layer.borderColor = UIColor.blackColor().CGColor
        
        self.hideKeyboardWhenTappedAround()

        currentUser = PFUser.currentUser()!
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func connectionButtonTapped(sender: AnyObject) {
        
        email = usernameField.text
        if validate(email){
            
            
            let userQuery = PFQuery(className: "_User")
            userQuery.whereKey("email", containsString: email)
            userQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
                
                if objects != nil{
                    
                    self.onlyUsers = objects! as [PFObject]
                    print(self.onlyUsers)
                    
                    if self.onlyUsers.count == 0{
                        
                        let alertController = UIAlertController(title: "User does not exist/Check email", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                        
                        let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                            
                            self.secondAlert()
                            
                        })
                        
                        alertController.addAction(defaultAction)
                        
                        self.presentViewController(alertController, animated: true, completion: nil)
                        
                    } else {
                        
                        for myOnlyUser in self.onlyUsers{
                            
                            let spouseRelation:PFRelation = self.currentUser.relationForKey("spouseRelation")
                            
                            spouseRelation.addObject(myOnlyUser as! PFObject)
                            
                            self.currentUser.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
                                if success{
                                    print("We saved")
                                    
                                    let query = spouseRelation.query()
                                    query.orderByAscending("username")
                                    query.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                                        
                                        if error != nil{
                                            print(error?.localizedDescription)
                                        } else {
                                            self.spouse = objects!
                                            
                                        }
                                        
                                        let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
                                        
                                        spouseRelation.addObject(user)
                                        self.recipeint.addObject(user.objectId!)
                                        
                                        self.sendRelationRequest()
                                    })
                                    
                                    
                                    
                                    
                                } else {
                                    print(error?.localizedDescription)
                                    
                                    
                                }
                            })
                            
                        }
                        
                        
                    }
                    
                    
                    
                    
                } else {
                    
                    print(error?.localizedDescription)
                }
                
            }
            

            
        } else {
            
            print("no valid email")
            
            let alertController = UIAlertController(title: "Email is not valid", message: "Please re-enter", preferredStyle: UIAlertControllerStyle.Alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                
               self.usernameField.text
                
            })
            alertController.addAction(defaultAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
        }
    
    func secondAlert() {
        
        let alertController = UIAlertController(title: "Invite your baby to join ReIgnite", message: "Tell them to join today!", preferredStyle: UIAlertControllerStyle.Alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            
            //email setup
           print(self.email)
            /*
            let url = NSURL(string: "mailto:\(self.email)")
            UIApplication.sharedApplication().openURL(url!)
            */
            
            let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
            self.presentViewController(vc, animated: true, completion: nil)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            //stay on same view to re-enter email address
        })
        alertController.addAction(defaultAction)
        alertController.addAction(cancelAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func sendRelationRequest(){
        let cUser = currentUser.username
        print(cUser)
        user = self.spouse.objectAtIndex(0) as! PFUser
        let myUser:String! = user.username! as String
        print(myUser)
        spouseConnectionReply = PFObject(className: "SpouseReply")
        spouseConnectionReply["connection"] = "connections"
        spouseConnectionReply["currentUser"] = cUser
        spouseConnectionReply["connectionStatus1"] = "accept"
        spouseConnectionReply["connectionStatus2"] = "decline"
        //spouseConnectionReply["recUser"] = myUser
        spouseConnectionReply.setObject(recipeint, forKey: "recipientsIds")
        spouseConnectionReply.setObject(PFUser.currentUser()!, forKey: "senderId")
        
        spouseConnectionReply.setObject(myUser, forKey: "other")
        spouseConnectionReply.setObject((PFUser.currentUser()?.username)!, forKey: "senderName")
        
        spouseConnectionReply.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
            
            if success{
                
                /*
                print(self.user)
                print(PFUser.currentUser()!)
                
               self.otherSpouseRelation = self.user.relationForKey("spouseRelation")
                self.recipeint.addObject(PFUser.currentUser()!)
                self.user.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) in
                    
                    if success{
                        print("YAY we have a relation...maybe")
                        
                        let acceptedConnection = PFObject(className: "AcceptedConnection")
                        acceptedConnection["accept"] = "accepted"
                        //acceptedConnection["comment"] = self.comments
                        print(self.recipeint)
                        //requested user object id
                        //self.recipeint.addObject(self.user.objectId!)
                        self.recipeint.addObject(self.currentUser.objectId!)
                        //current user object
                        
                        //self.spouseRecipient.addObject(self.currentUser.objectId!)
                        self.spouseRecipient.addObject(self.user.objectId!)
                        print("Recipient=\(self.recipeint)")
                        acceptedConnection.setObject(self.recipeint, forKey: "recipientsIds")
                        acceptedConnection.setObject(self.spouseRecipient, forKey: "spouseRecipientsId")
                        acceptedConnection.setObject(PFUser.currentUser()!, forKey: "senderId")
                        acceptedConnection.setObject(self.currentUser.objectId!, forKey: "spouseSenderId")
                        acceptedConnection.setObject((PFUser.currentUser()?.username)!, forKey: "senderName")
                        //acceptedConnection.setObject(<#T##object: AnyObject##AnyObject#>, forKey: <#T##String#>)
                        
                        acceptedConnection.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
                            
                            if success{
                                //remove the connection request object
                                //self.selectedMessage = self.messages.objectAtIndex(0) as! PFObject
                                print("success acceptance connection")
                                
                            } else{
                                print(error?.localizedDescription)
                            }
                        }

                    } else {
                        print(error?.localizedDescription)
                    }
                })
                */
                
                let alertController = UIAlertController(title: "Success", message: "Request has been sent", preferredStyle: UIAlertControllerStyle.Alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                    
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                    self.presentViewController(vc, animated: true, completion: nil)
                    
                })
                alertController.addAction(defaultAction)
                self.presentViewController(alertController, animated: true, completion: nil)
                
            } else {
                
                let alertController = UIAlertController(title: "Error", message: "Please try again", preferredStyle: UIAlertControllerStyle.Alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                    
                    print(error?.localizedDescription)
                    
                })
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                    
                    let controller = self.storyboard!.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
                    
                    let controllerNav = UINavigationController(rootViewController: controller)
                    
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    appDelegate.window?.rootViewController = controllerNav
                    
                    
                })
                alertController.addAction(defaultAction)
                alertController.addAction(cancelAction)
                self.presentViewController(alertController, animated: true, completion: nil)
                
                
            }
        }
    }

  
    
    func validate(YourEMailAddress: String) -> Bool {
        let REGEX: String
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluateWithObject(YourEMailAddress)
    }
    
    
}
