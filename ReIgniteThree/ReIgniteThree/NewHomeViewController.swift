//
//  NewHomeViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/28/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class NewHomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    var date:NSArray = []
    var nDate:String!
    var nTime:String!
    var nLocation:String!
    var nAttire:String!
    var myDate:String!
    var spouseRelation:PFRelation = PFRelation()
    var spouseObjects:NSArray = []
    var mySpouseArray:NSArray = []
    var spouse:NSArray = []
    var acceptedArray:NSArray = []
    var random:Int!
    var picMessagetimer: NSTimer!
    var connectWithBabyTimer: NSTimer!
    var spousesReply:NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        connectionReplyTimer()
        

        pictureTimer()
       messagesButton()
        tableView.delegate = self
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        
        
        if let nav = navigationController?.navigationBar{
            
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            
            
        }

        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        let sQuery:PFQuery = PFQuery(className: "SpouseReply")
        sQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        sQuery.orderByAscending("createdAt")
        sQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if objects != nil {
                
                self.mySpouseArray = objects!
                print(self.mySpouseArray.count)
                print(self.mySpouseArray)
                
                if self.mySpouseArray.count == 1 {
                    //cool to enter initialize spouseRelation
                    self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                    let squery = self.spouseRelation.query()
                    squery.orderByAscending("username")
                    squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                        
                        if error != nil{
                            print(error?.localizedDescription)
                        } else {
                            self.spouse = objects!
                            
                        }
                        
                        let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
                        
                        if user.objectId != nil{
                            //have a user hide connection button
                            self.connectWithBabyTimer.invalidate()
                            
                        } else {
                            self.connectionReplyTimer()
                        }
                        
                        //self.recipient.addObject(user.objectId!)
                    })
                    
                } else {
                    //check if you have an accepted connection
                    
                    let aQuery:PFQuery = PFQuery(className: "AcceptedConnection")
                    aQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
                    aQuery.orderByAscending("createdAt")
                    aQuery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?, error:NSError?) in
                        
                        if objects != nil {
                            self.acceptedArray = objects!
                            if self.acceptedArray.count == 1{
                                //yay they can do their thang
                                
                                self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                                let squery = self.spouseRelation.query()
                                squery.orderByAscending("username")
                                squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                                    
                                    if error != nil{
                                        print(error?.localizedDescription)
                                    } else {
                                        self.spouse = objects!
                                        
                                    }
                                    
                                    let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
                                    
                                    if user.objectId != nil{
                                        //have a user hide connection button
                                         self.connectWithBabyTimer.invalidate()
                                        
                                    } else {
                                        self.connectionReplyTimer()
                                    }
                                    
                                    //self.recipient.addObject(user.objectId!)
                                })
                                
                            } else {
                                
                                print("no connections yet")
                                //change text in "View a Date"
                                /*
                                let alertController = UIAlertController(title: "Sorry! You have to wait till your baby connects", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                                
                                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                                    
                                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                                    let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                                    self.presentViewController(vc, animated: true, completion: nil)
                                    
                                })
                                alertController.addAction(defaultAction)
                                self.presentViewController(alertController, animated: true, completion: nil)
                                */
                            }
                        } else {
                            print(error?.localizedDescription)
                        }
                    })
                    
                    
                    
                }
            }
        }
        
        let dateQuery:PFQuery = PFQuery(className:"DateRequest")
        dateQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        dateQuery.whereKey("status", notEqualTo: "accepted")
        dateQuery.orderByDescending("createdAt")
        dateQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else{
                //we have a message
                self.date = objects!
                print(self.date)
                print(self.date.count)
                self.tableView.reloadData()
                //let dates:PFObject = self.date.objectAtIndex(0) as! PFObject
            }
        }


    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return date.count
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        print("index:\(indexPath.row)")
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        let dates:PFObject = date.objectAtIndex(indexPath.row) as! PFObject
        nDate = dates.objectForKey("datePicker") as? String
        nLocation = dates.objectForKey("location") as? String
        nAttire = dates.objectForKey("dressType") as? String
        
        let myCount:String? = nDate
        print(myCount)
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("DateHistoryDetailViewController") as! DateHistoryDetailViewController
        let controllerNav = UINavigationController(rootViewController: controller)
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        if let myDate = nDate{
            print(myDate)
            controller.theIndex = random
            controller.theDate = myDate
            controller.theLocation = nLocation
            controller.theAttire = nAttire
            appDelegate.window?.rootViewController = controllerNav
        } else {
            print("no date")
        }
        
        //controller.theTime = nTime

    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell:DateHistoryTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! DateHistoryTableViewCell
        
        
        
        print(self.date.count)
        let dates:PFObject = date.objectAtIndex(indexPath.row) as! PFObject
        nLocation = dates.objectForKey("location") as? String
        //nTime = dates.objectForKey("time") as? String
        nDate = dates.objectForKey("datePicker") as? String
        nAttire = dates.objectForKey("dressType") as? String
        
        
        if let newLocation = nLocation {
            let myLocation = newLocation
            print(myLocation)
        }
        if let newDate = nDate {
            myDate = newDate
            print(myDate)
        }
        if let newAttire = nAttire{
            let myAttire = newAttire
            print(myAttire)
            
            
        } else {
            print("we have nothing in the request")
            
            //cell.timeLabel.hidden = true
            cell.placeLabel.hidden = true
            cell.dateLabel.hidden = true
            cell.attireLabel.hidden = true
            cell.location.hidden = true
            //cell.time.hidden = true
            cell.date.hidden = true
            cell.attire.hidden = true
        }
        
        
        cell.location?.text =  nLocation
        //cell.time?.text = nTime
        cell.date?.text = nDate
        cell.attire?.text = nAttire

        
        return cell

        
    }
    
    @IBAction func newMessageButtonTapped(sender: AnyObject) {
        
        picMessagetimer.invalidate()
        
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("MessageInboxViewController") as! MessageInboxViewController
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window?.rootViewController = controllerNav
        
        
    }
    
    func messagesButton() {
        messageButton.setTitle("No video messages to view", forState: .Normal)
        messageButton.layer.cornerRadius = 5
        messageButton.layer.borderWidth = 1
        messageButton.layer.borderColor = UIColor.blackColor().CGColor
        
        messageButton.enabled = false
        messageButton.backgroundColor = UIColor.grayColor()
       
    }
    
    func checkForNewPic() {
        
        let query:PFQuery = PFQuery(className:"Messages")
        query.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        //query.whereKey("senderName", containsString: PFUser.currentUser()?.username)
        query.orderByDescending("createdAt")
        query.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else{
                
                var messages:NSArray = []
                messages = objects!
                if messages == [] {
                    
                    self.messageButton.enabled = false
                    self.messageButton.backgroundColor = UIColor.grayColor()
                    
                } else {
                    
                    self.messageButton.enabled = true
                    self.messageButton.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
                    self.messageButton.setTitle("You received a new message!", forState: .Normal)
                    
                }
                
                
            }
        }

    }
    
    func pictureTimer() {
        picMessagetimer =  NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: #selector(NewHomeViewController.checkForNewPic), userInfo: nil, repeats: false)
    }
    
    func connectionReplyTimer() {
        connectWithBabyTimer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: #selector(NewHomeViewController.spouseReply), userInfo: nil, repeats: false)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        //picMessagetimer.invalidate()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        picMessagetimer.invalidate()
    }
    
    func spouseReply(){
        
        
        
        let spouseQuery:PFQuery = PFQuery(className:"SpouseReply")
        
        spouseQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        spouseQuery.orderByDescending("createdAt")
        spouseQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else{
                
                self.spousesReply = objects!
                print(self.spousesReply.count)
                if self.spousesReply.count >= 1{
                    
                    //self.connectButton.enabled = true
                    /*
                    self.navigationItem.rightBarButtonItem?.tintColor = UIColor.grayColor()
                    
                    let notification = UILocalNotification()
                    notification.fireDate = NSDate(timeIntervalSinceNow: 5)
                    notification.alertBody = "Hey you! Yeah you! You have a connection request!"
                    notification.alertAction = "Check it out!"
                    notification.soundName = UILocalNotificationDefaultSoundName
                    notification.userInfo = ["CustomField1": "w00t"]
                    UIApplication.sharedApplication().scheduleLocalNotification(notification)
                    */
                    
                    let alertController = UIAlertController(title: "Your baby wants to connect with you", message: "Check it out now!", preferredStyle: UIAlertControllerStyle.Alert)
                    let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                        
                        //self.selectedMessage = self.messages.objectAtIndex(0) as! PFObject
                        
                        
                        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("ConnectionReplyViewController") as! ConnectionReplyViewController
                        
                        let controllerNav = UINavigationController(rootViewController: controller)
                        
                        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        appDelegate.window?.rootViewController = controllerNav
                        
                    })
                    
                    alertController.addAction(defaultAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                    
                    self.connectWithBabyTimer.invalidate()
                    
                    
                    //self.navigationController?.popViewControllerAnimated(true)
                    /*
                     let alertController = UIAlertController(title: "New Connection", message: "Check it out!", preferredStyle: UIAlertControllerStyle.Alert)
                     let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                     
                     //self.selectedMessage = self.messages.objectAtIndex(0) as! PFObject
                     
                     
                     let controller = self.storyboard!.instantiateViewControllerWithIdentifier("ConnectionReplyViewController") as! ConnectionReplyViewController
                     
                     let controllerNav = UINavigationController(rootViewController: controller)
                     
                     let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                     appDelegate.window?.rootViewController = controllerNav
                     
                     })
                     
                     alertController.addAction(defaultAction)
                     self.presentViewController(alertController, animated: true, completion: nil)
                     */
                    
                } else {
                    print("no spouse request")
                }
                
            }
            
        }
        
    }

    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.connectWithBabyTimer.invalidate()
        self.picMessagetimer.invalidate()
    }
   
}
