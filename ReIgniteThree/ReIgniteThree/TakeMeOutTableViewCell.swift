//
//  TakeMeOutTableViewCell.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/20/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit

class TakeMeOutTableViewCell: UITableViewCell {

    @IBOutlet var name: UILabel!
    @IBOutlet var address: UILabel!
    @IBOutlet var number: UILabel!
    @IBOutlet var rating: CosmosView!
   
}
