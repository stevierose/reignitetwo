//
//  FavoriteDetailViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/26/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import GoogleMaps
import Parse

class FavoriteDetailViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    
    var name:String!
    var address:String!
    var number:String!
    var lat:String!
    var lng:String!
    var nLat:CLLocationDegrees = 0.0
    var nLng:CLLocationDegrees = 0.0
    var theName:String!
    var theAddress:String!
    var theNumber:String!
    var theLat:String!
    var theLng:String!
    var favObjId:NSArray = []
    var objectId:String!
    
    var didFindMyLocation = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let nav = navigationController?.navigationBar{
            
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            
        }
        
        if let myName = name{
            nameLabel.text = myName
            theName = myName
        } else {
            print("no name")
        }
        
        if let myAddress = address{
            addressLabel.text = myAddress
            theAddress = myAddress
        } else {
            print("no address")
        }
        
        if let myNumber = number {
            
            numberLabel.text = myNumber
            theNumber = myNumber
            
        } else {
            print("no number")
        }
        
        if let myNlat = lat{
            
            nLat = (myNlat as NSString).doubleValue
            print(nLat)
            theLat = String(nLat)
        } else {
            print("no lat")
        }
        
        if let myNlng = lng{
            
            nLng = (myNlng as NSString).doubleValue
            print(nLng)
            theLng = String(nLng)
        } else {
            print("no lng")
        }
        
        print(nLng)
        print(nLat)

        
        
        let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(nLat, longitude: nLng, zoom: 8.0)
        mapView.camera = camera


        
    }


    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.AuthorizedWhenInUse {
            mapView.myLocationEnabled = true
        }
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        
        if !didFindMyLocation {
            let myLocation: CLLocation = change![NSKeyValueChangeNewKey] as! CLLocation
            
            mapView.camera = GMSCameraPosition.cameraWithTarget(myLocation.coordinate, zoom: 10.0)
            mapView.settings.myLocationButton = true
            
            didFindMyLocation = true
        }
    }
    

    @IBAction func backButtonTapped(sender: AnyObject) {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("FavSWRevealViewController") as! SWRevealViewController
        self.presentViewController(vc, animated: true, completion: nil)
        
       

    }
    

    @IBAction func deleteFavButtonTapped(sender: AnyObject) {
        
        
        
        
        
        
        
        
        
    }
}
