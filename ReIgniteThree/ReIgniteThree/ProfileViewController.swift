//
//  ProfileViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/16/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class ProfileViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var confirmPasswordField: UITextField!
    @IBOutlet var menuButton: UIBarButtonItem!
    @IBOutlet weak var updateButton: UIButton!
    
    var img:UIImage? = nil
    var pictures:NSArray = []
    var  cUser:PFUser = PFUser()
    var picture:PFObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateButton.layer.cornerRadius = 5
        updateButton.layer.borderWidth = 1
        updateButton.layer.borderColor = UIColor.blackColor().CGColor
        
        self.hideKeyboardWhenTappedAround()
        img = UIImage()
        
        imageView.layer.borderWidth = 1
        imageView.layer.masksToBounds = false
        imageView.layer.borderColor = UIColor.blackColor().CGColor
        imageView.layer.cornerRadius = imageView.frame.height / 2
        imageView.clipsToBounds = true
        
        let query:PFQuery = PFQuery(className:"Gallery")
        let user = PFUser.currentUser()!
        print(user)
        query.whereKey("sender", containsString: PFUser.currentUser()?.username)
        //query.whereKey("sender", equalTo: (PFUser.currentUser()?.username)!)
        query.orderByDescending("createdAt")
        query.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else{
                //we have a message
                self.pictures = objects!
                print(self.pictures.count)
                self.pictures.objectAtIndex(0)
                self.picture = self.pictures.objectAtIndex(0) as! PFObject
                let imageFile:PFFile = self.picture.objectForKey("imageFile") as! PFFile
                let imageFileUrl:NSURL = NSURL(string: imageFile.url!)!
                let imageData:NSData = (NSData(contentsOfURL: imageFileUrl))!
                self.imageView.image = UIImage(data: imageData)!
                print(self.imageView.image)
                
                
            }
        }

        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        

        if let nav = navigationController?.navigationBar{
            //UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.grayColor()]
            
            
        }
       
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        //when view loads retrieve user picture
        
        
    }

    
    @IBAction func editButtonTapped(sender: AnyObject) {
        //pull images from photo library
        
        let image = UIImagePickerController()
        image.delegate = self
        image.allowsEditing = false
        image.sourceType  = UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(image, animated: true, completion: nil)
        
        
    }

    @IBAction func updateButtonTapped(sender: AnyObject) {
        //save image to parse
        //optional save image, email, password 
        
        unamePwordUpdate()
        self.upLoadPic()
        
    }
    
    func unamePwordUpdate() {
        
        var email:String!
        var password:String!
        var cPassword:String!
        //var username:String!
        
        
        email = emailField.text
        password = passwordField.text
        cPassword = confirmPasswordField.text
        
        //let user = PFUser()

        cUser = PFUser.currentUser()!
        print(cUser)

        if email.characters.count > 3{
            
            if validate(email){
                //is valid
                print("valid")
                cUser.email = email
                
                cUser.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
                    
                    if success{
                        
                        
                        self.emailField.text = ""
                        self.passwordField.text = ""
                        self.confirmPasswordField.text = ""
                        
                        let alertController = UIAlertController(title: "Success", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                        
                        let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                            
                        })
                        alertController.addAction(defaultAction)
                        self.presentViewController(alertController, animated: true, completion: nil)
                        
                        
                    } else {
                        //alert username and password did not update
                        
                        let alertController = UIAlertController(title: "Username and/or password did not update", message: "Please re-enter", preferredStyle: UIAlertControllerStyle.Alert)
                        
                        let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                            
                        })
                        alertController.addAction(defaultAction)
                        self.presentViewController(alertController, animated: true, completion: nil)
                        print(error?.localizedDescription)
                    }
                }

                
                
                
            } else {
                //alert invalid email
                let alertController = UIAlertController(title: "Invalid email", message: "Please re-enter", preferredStyle: UIAlertControllerStyle.Alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                    
                    self.emailField.text = ""
                    self.passwordField.text = ""
                    self.confirmPasswordField.text = ""
                    
                })
                alertController.addAction(defaultAction)
                self.presentViewController(alertController, animated: true, completion: nil)
                
                
            }

            
        } else {
            print("chose to not enter email")
        }
        if password.characters.count > 3{
            
            if password == cPassword{
                
                cUser = PFUser.currentUser()!
                cUser.password = cPassword
                
                cUser.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
                    
                    if success{
                        
                        
                        self.emailField.text = ""
                        self.passwordField.text = ""
                        self.confirmPasswordField.text = ""
                        
                        let alertController = UIAlertController(title: "Success", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                        
                        let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                            
                        })
                        alertController.addAction(defaultAction)
                        self.presentViewController(alertController, animated: true, completion: nil)
                        
                        
                    } else {
                        //alert username and password did not update
                        
                        let alertController = UIAlertController(title: "Username and/or password did not update", message: "Please re-enter", preferredStyle: UIAlertControllerStyle.Alert)
                        
                        let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                            
                        })
                        alertController.addAction(defaultAction)
                        self.presentViewController(alertController, animated: true, completion: nil)
                        print(error?.localizedDescription)
                    }
                }
                
                
                
                
            } else {
                
                let alertController = UIAlertController(title: "Password does not match", message: "Please re-enter", preferredStyle: UIAlertControllerStyle.Alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                    
                })
                alertController.addAction(defaultAction)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
            
        } else {
            
            print("no password")
        }

       
        
        

    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        let theInfo:NSDictionary = info as NSDictionary
        
         img = theInfo.objectForKey(UIImagePickerControllerOriginalImage) as? UIImage
        
        UIImageWriteToSavedPhotosAlbum(img!, nil, nil, nil)
        
        print(img)
        
        imageView.image = img
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func upLoadPic() {
        /*
        let pic = PFObject(className:"PhotoGallery")
        print(img)
        /*
        let newImage = resizeImage(img, width: 750.0, height: 1294.0)
        let imageData = UIImagePNGRepresentation(newImage)
        let imageFile = PFFile(name: "image.png", data: imageData!)
        */
        pic["imageName"] = "userImage"
        //pic["imageFile"] = imageFile
        pic["fileType"] = "image"
        
        //print(cUser.username)
        //pic.setObject((cUser.username)!, forKey: "senderName")
        pic.saveInBackgroundWithBlock { (success:Bool, error:NSError?) -> Void in
            
            if success{
                
                let alertController = UIAlertController(title: "Picture saved", message: "Thank you", preferredStyle: UIAlertControllerStyle.Alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                    
                })
                alertController.addAction(defaultAction)
                self.presentViewController(alertController, animated: true, completion: nil)
                
            } else {
                //alert picture did not upload
                
                let alertController = UIAlertController(title: "Picture did not update", message: "Please re-try", preferredStyle: UIAlertControllerStyle.Alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                    
                })
                alertController.addAction(defaultAction)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
      */
        
        let gallery = PFObject(className: "Gallery")
        
            //determine what device the phone is running one
            //add key to look
            
        
        
        let newImage = resizeImage(img!, width: 750.0, height: 1294.0)
        let imageData = UIImagePNGRepresentation(newImage)
        let imageFile = PFFile(name: "image.png", data: imageData!)
        
        gallery["imageName"] = "Sexy Image"
        gallery["imageFile"] = imageFile
        gallery["fileType"] = "image"
        
        gallery.setObject(cUser.username!, forKey: "sender")
      
        gallery.saveInBackgroundWithBlock({ (success:Bool, error:NSError?) -> Void in
            if success{
                print("YAY")
                
            } else {
                print("boo")
            }
        })

        
    }
    
    func resizeImage(image:UIImage, width:CGFloat, height:CGFloat)->UIImage{
        
        let newSize = CGSizeMake(width, height)
        let newRectangle = CGRectMake(0, 0, width, height)
        UIGraphicsBeginImageContext(newSize)
        image.drawInRect(newRectangle)
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resizedImage
        
    }
    
    func validate(YourEMailAddress: String) -> Bool {
        let REGEX: String
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluateWithObject(YourEMailAddress)
    }
    
    

    @IBAction func backButtonTapped(sender: AnyObject) {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func isValidPassword(candidate:String!)->Bool{
        
        let passwordRegex = "(?=.*[a-z])(?=.*[A-Z](?=.*\\d).{6,10}"
        
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluateWithObject(candidate)
    }
    
    

    
}
