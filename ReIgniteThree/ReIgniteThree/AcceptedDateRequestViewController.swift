//
//  AcceptedDateRequestViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/15/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class AcceptedDateRequestViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate {
    
    var spouseRelation:PFRelation = PFRelation()
    var recipient:NSMutableArray = []
    var spouse:NSArray = []
    var date:NSArray = []
    var user:PFUser = PFUser()
    var theUser:PFUser = PFUser()
    var status:String!
    var comment:String!
    var ddate:String!
    var location:String!
    var time:String!
    var counter = 0
    var confirmedDate:String!
    var confirmedDressType:String!
    var confirmedLocation:String!
    
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        
        if let nav = navigationController?.navigationBar{
          
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            
        }
        
        spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
        let query = spouseRelation.query()
        query.orderByAscending("username")
        query.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else {
                self.spouse = objects!
                
            }
            
            self.user = self.spouse.objectAtIndex(0) as! PFUser
            self.theUser = self.user
            print(self.user.objectId)
            self.recipient.addObject(self.user.objectId!)
        })
        
      
       print("myuser\(theUser.objectId)")
        
        let dateQuery:PFQuery = PFQuery(className:"DateRequest")
        dateQuery.whereKey("status", containsString: "accepted")
        dateQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        //dateQuery.whereKey("senderName", equalTo: (PFUser.currentUser()?.username)!)
        dateQuery.orderByDescending("createdAt")
        dateQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else{
                //we have a message
                self.date = objects!
                print(self.date.count)
                self.tableView.reloadData()
                //let dates:PFObject = self.date.objectAtIndex(0) as! PFObject
            }
        }
            //Accepted request for my baby
        /*
        let sdateQuery:PFQuery = PFQuery(className:"DateRequest")
        sdateQuery.whereKey("status", containsString: "accepted")
        //sdateQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        sdateQuery.whereKey("senderName", equalTo: (PFUser.currentUser()?.username)!)
        sdateQuery.orderByDescending("createdAt")
        sdateQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else{
                //we have a message
                self.date = objects!
                print(self.date.count)
                self.tableView.reloadData()
                //let dates:PFObject = self.date.objectAtIndex(0) as! PFObject
            }
        }

        */

    }

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:AcceptedDateTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! AcceptedDateTableViewCell
        
        
        let dates:PFObject = date.objectAtIndex(indexPath.row) as! PFObject
        //print(cell.status.text?.characters.count)
        if cell.status.text?.characters.count < 1{
            print("no status must be other user")
        } else {
            print("status")
            
            
            ddate = dates.objectForKey("datePicker") as? String
            location = dates.objectForKey("location") as? String
            status = dates.objectForKey("status") as? String
            //time = dates.objectForKey("time") as? String
            comment = dates.objectForKey("dateComments") as? String
            print(comment)
            confirmedDate = dates.objectForKey("confirmedDate") as? String
            confirmedLocation = dates.objectForKey("confirmedLocation") as? String
            confirmedDressType = dates.objectForKey("confirmedDressType") as? String
            print(confirmedDressType)
            print(confirmedLocation)
            print(confirmedDate)
            
            
            if let theDate = confirmedDate{
                
                let myDate = theDate
                if myDate == ""{
                    
                    print("no date")
                    ddate = ""
                }else {
                    
                    cell.date.text = myDate
                }
                
            } else {
                print("no date")
            }
       
            if let theLocation = confirmedLocation{
                
                let myLocation = theLocation
                
                if myLocation == ""{
                    print("no location")
                    location = ""
                } else {
                    cell.location.text = myLocation
                }
            } else {
                print("no locale")
            }
            
            if let theStatus = confirmedDressType{
                
                let myStatus = theStatus
               // cell.status.text = myStatus
                if myStatus == ""{
                    
                    print("no status")
                    status = ""
                } else {
                    cell.status.text = myStatus
                }
            } else {
                print("no stat-us")
            }
           
            if let theComment = comment{
                
                let myComment = theComment
                if myComment == ""{
                    
                    print("no comment listed")
                    comment = ""
                } else {
                    
                    cell.comments.text = myComment
                }
            } else {
                print("no comment please")
            }
         
           
            /*
            cell.status.text = status
            cell.location.text = location
            cell.date.text = ddate
            //cell.time.text = time
            cell.comments.text = comment
            */

        }
            //print(dates.objectForKey("date") as? String)
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        print("user tapped on row: \(indexPath.row)")
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return date.count
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("DateHistoryViewController") as! DateHistoryViewController
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window?.rootViewController = controllerNav

        
    }
    
}
