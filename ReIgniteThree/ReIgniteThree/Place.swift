//
//  Place.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/22/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import Foundation

class Place {
    
    var name = ""
    var rating = ""
    var phoneNumber = ""
    var address = ""
    var lat:Double = 0.0;
    var lng:Double = 0.0;
    var newName:String!
    var newRating:String!
    var newPhoneNumber:String!
    var newAddress:String!
    var newLat:Double = 0.0
    var newLng:Double = 0.0
    
   
    var personName:(String!){
        
        get{
            return name
        } set(newVal) {
            
            newName = newVal
            
        }
    }
    
    var personRating:(String!) {
        
        get{
            return rating
        } set(newVal){
            
            newRating = newVal
        }
        
    }
    
    var personNumber:(String!) {
        get{
          return phoneNumber
            
        } set(newVal){
            newPhoneNumber = newVal
        }
    }
    
    var personAddress:(String!) {
        
        get{
            return address
            
        } set(newVal){
        
            newAddress = newVal
        }
    }
    
    var personLat:(Double!) {
        
        get{
            return lat
        } set(newVal) {
            newLat = newVal
        }
    }
    
    var personLng:(Double!) {
        
        get{
            return lng
        } set(newVal) {
            newLng = newVal
        }
    }
    
    
    
    
    
    
}