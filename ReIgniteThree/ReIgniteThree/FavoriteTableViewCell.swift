//
//  FavoriteTableViewCell.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/23/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit

class FavoriteTableViewCell: UITableViewCell {

  
    @IBOutlet var name: UILabel!
    @IBOutlet var address: UILabel!
    @IBOutlet var number: UILabel!
  
}
