//
//  PartnersAcceptedViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/30/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class PartnersAcceptedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    var date:NSArray = []
    var spouseRelation:PFRelation = PFRelation()
    var confirmedDate:String!
    var confirmedDressType:String!
    var confirmedLocation:String!
    var comment:String!
    var spouse:NSArray = []
    var user:PFUser = PFUser()
    var theUser:PFUser = PFUser()
    var recipient:NSMutableArray = []
    var mySpouseArray:NSArray = []
    var acceptedArray:NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        
        /*
        let fQuery:PFQuery = PFQuery(className: "SpouseReply")
        //fQuery.whereKey("other", containsString: PFUser.currentUser()?.username)
        fQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) in
            
            if objects != nil {
                
            }
            
        }
        
        
        
        
        print("name doesnt match")
        
        
        let sQuery:PFQuery = PFQuery(className: "SpouseReply")
        sQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
        //sQuery.whereKey("recUser", containsString: PFUser.currentUser()?.username)
        sQuery.orderByAscending("createdAt")
        sQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
            
            if objects != nil {
                
                
                self.mySpouseArray = objects!
                print(self.mySpouseArray.count)
                print(self.mySpouseArray)
                
                
                
                if self.mySpouseArray.count == 1 {
                    //cool to enter initialize spouseRelation
                    self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                    let squery = self.spouseRelation.query()
                    squery.orderByAscending("username")
                    squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                        
                        if error != nil{
                            print(error?.localizedDescription)
                        } else {
                            self.spouse = objects!
                            
                        }
                        
                        let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
                        
                        if user.objectId != nil{
                            //have a user hide connection button
                            
                            
                        } else {
                            
                        }
                        
                        self.recipient.addObject(user.objectId!)
                    })
                    
                } else {
                    //check if you have an accepted connection
                    
                    let aQuery:PFQuery = PFQuery(className: "AcceptedConnection")
                    aQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
                    aQuery.orderByAscending("createdAt")
                    aQuery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?, error:NSError?) in
                        
                        if objects != nil {
                            self.acceptedArray = objects!
                            if self.acceptedArray.count >= 1{
                                //yay they can do their thang
                                
                                self.spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
                                let squery = self.spouseRelation.query()
                                squery.orderByAscending("username")
                                squery.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
                                    
                                    if error != nil{
                                        print(error?.localizedDescription)
                                    } else {
                                        self.spouse = objects!
                                        
                                    }
                                    
                                    let user:PFUser = self.spouse.objectAtIndex(0) as! PFUser
                                    
                                    if user.objectId != nil{
                                        //have a user hide connection button
                                        
                                        
                                    } else {
                                        
                                    }
                                    
                                    self.recipient.addObject(user.objectId!)
                                })
                                
                            } else {
                                let alertController = UIAlertController(title: "Sorry! You have to wait till your baby connects", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                                
                                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                                    
                                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                                    let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                                    self.presentViewController(vc, animated: true, completion: nil)
                                    
                                })
                                alertController.addAction(defaultAction)
                                self.presentViewController(alertController, animated: true, completion: nil)
                            }
                        } else {
                            print(error?.localizedDescription)
                        }
                    })
                    
                    
                    
                    
                    /*
                     let alertController = UIAlertController(title: "Sorry! You have to wait till your baby connects", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                     
                     let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                     
                     let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                     let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                     self.presentViewController(vc, animated: true, completion: nil)
                     
                     })
                     alertController.addAction(defaultAction)
                     self.presentViewController(alertController, animated: true, completion: nil)
                     */
                }
                
            } else {
                print(error?.localizedDescription)
            }
        }
        

        */
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

        if let nav = navigationController?.navigationBar{
            
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            
        }
        
        spouseRelation = PFUser.currentUser()?.objectForKey("spouseRelation") as! PFRelation
        
        let query = spouseRelation.query()
        query.orderByAscending("username")
        query.findObjectsInBackgroundWithBlock({ (objects:[PFObject]?,error: NSError?) -> Void in
            
            if error != nil{
                print(error?.localizedDescription)
            } else {
                self.spouse = objects!
                
            }
            
            self.user = self.spouse.objectAtIndex(0) as! PFUser
            self.theUser = self.user
            print(self.user.objectId)
            self.recipient.addObject(self.user.objectId!)
        })

        //Accepted request for my baby
        
         let sdateQuery:PFQuery = PFQuery(className:"DateRequest")
         sdateQuery.whereKey("status", containsString: "accepted")
         //sdateQuery.whereKey("recipientsIds", equalTo: (PFUser.currentUser()?.objectId)!)
         sdateQuery.whereKey("senderName", equalTo: (PFUser.currentUser()?.username)!)
         sdateQuery.orderByDescending("createdAt")
         sdateQuery.findObjectsInBackgroundWithBlock { (objects:[PFObject]?, error:NSError?) -> Void in
         
         if error != nil{
         print(error?.localizedDescription)
         } else{
         //we have a message
         self.date = objects!
         print(self.date.count)
         self.tableView.reloadData()
         //let dates:PFObject = self.date.objectAtIndex(0) as! PFObject
         }
         }
         
 
        


        // Do any additional setup after loading the view.
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
   
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return date.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print(indexPath.row)
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:PartnersAcceptTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! PartnersAcceptTableViewCell
        
        let dates:PFObject = date.objectAtIndex(indexPath.row) as! PFObject
        
        
        comment = dates.objectForKey("dateComments") as? String
        confirmedDate = dates.objectForKey("confirmedDate") as? String
        confirmedLocation = dates.objectForKey("confirmedLocation") as? String
        confirmedDressType = dates.objectForKey("confirmedDressType") as? String
        print(comment)
        print(confirmedDressType)
        
        cell.comments.text = comment
        cell.date.text = confirmedDate
        cell.dressType.text = confirmedDressType
        cell.location.text = confirmedLocation
        
        
        
        
        return cell
    }


}
