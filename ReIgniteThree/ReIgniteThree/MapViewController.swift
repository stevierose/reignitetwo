//
//  MapViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/22/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Foundation
import GoogleMaps
import Parse



class MapViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    
    var name:String!
    var address:String!
    var number:String!
    var lat:String!
    var lng:String!
    var nLat:CLLocationDegrees = 0.0
    var nLng:CLLocationDegrees = 0.0
    var theName:String!
    var theAddress:String!
    var theNumber:String!
    var theLat:String!
    var theLng:String!
    
    @IBOutlet weak var favoriteButton: UIButton!
    
    var placeId:String!
    let APIKEY = "AIzaSyCU0ybMB-w_7teJ-ijF4TBkPW4KY--CBUI"
    var placeIdUrlString:String!
    var locationManager = CLLocationManager()
    
    var didFindMyLocation = false
    
   
    
    //self.placeIdUrlString = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + self.placeId + "&key=" + self.APIKEY + ""
    
    @IBOutlet var mapView: GMSMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        favoriteButton.layer.cornerRadius = 5
        favoriteButton.layer.borderWidth = 1
        favoriteButton.layer.borderColor = UIColor.blackColor().CGColor
        
        if let nav = navigationController?.navigationBar{
            
            nav.backgroundColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.barTintColor = UIColor(red: 0.62, green: 0.06, blue: 0.04, alpha: 1.0)
            nav.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            
            
        }
        
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        //Current location
        //lat = locationManager.location?.coordinate.latitude
        //lng = locationManager.location?.coordinate.longitude
     
        if let myName = name{
            nameLabel.text = myName
            theName = myName
        } else {
            print("no name")
        }
        
        if let myAddress = address{
            addressLabel.text = myAddress
            theAddress = myAddress
        } else {
            print("no address")
        }
        
        if let myNumber = number {
            
            numberLabel.text = myNumber
            theNumber = myNumber
            
        } else {
            print("no number")
        }
        
        if let myNlat = lat{
            
            nLat = (myNlat as NSString).doubleValue
            print(nLat)
            theLat = String(nLat)
        } else {
            print("no lat")
        }
        
        if let myNlng = lng{
            
            nLng = (myNlng as NSString).doubleValue
            print(nLng)
            theLng = String(nLng)
        } else {
            print("no lng")
        }
        
        print(nLng)
        print(nLat)
       
        
        let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(nLat, longitude: nLng, zoom: 8.0)
        mapView.camera = camera
        
       // mapView.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.New, context: nil)
    
    }



    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.AuthorizedWhenInUse {
            mapView.myLocationEnabled = true
        }
    }
    
    
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        
        if !didFindMyLocation {
            let myLocation: CLLocation = change![NSKeyValueChangeNewKey] as! CLLocation
    
            mapView.camera = GMSCameraPosition.cameraWithTarget(myLocation.coordinate, zoom: 10.0)
            mapView.settings.myLocationButton = true
            
            didFindMyLocation = true
        }
    }
    
    @IBAction func backButtonTapped(sender: AnyObject) {
      
        
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("TakeMeOutViewController") as! TakeMeOutViewController
        self.presentViewController(vc, animated: true, completion: nil)

    }
   
    @IBAction func saveFavButtonTapped(sender: AnyObject) {
        
        let query = PFObject(className: "Favorite")
        query["name"] = theName
        query["address"] = theAddress
        query["number"] = theNumber
        query["lat"] = theLat
        query["lng"] = theLng
        query["cUser"] = PFUser.currentUser()?.username
        query["userId"] = PFUser.currentUser()?.objectId
        query.saveInBackgroundWithBlock { (success:Bool, error:NSError?) in
            
            if success {
                print("success")
                let alertController = UIAlertController(title: "The \(self.theName) has been saved", message: "Continue to ReIgnite", preferredStyle: UIAlertControllerStyle.Alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                    
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
                    let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                    self.presentViewController(vc, animated: true, completion: nil)

                    
                })
                alertController.addAction(defaultAction)
                self.presentViewController(alertController, animated: true, completion: nil)
                
            } else {
                print(error?.localizedDescription)
                
                
            }
        }

        /*
        let controller = self.storyboard!.instantiateViewControllerWithIdentifier("FavoriteViewController") as! FavoriteViewController
        
        let controllerNav = UINavigationController(rootViewController: controller)
        
        controller.name = theName
        controller.number = theNumber
        controller.address = theAddress
        controller.lat = theLat
        controller.lng = theLng
        
        
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window?.rootViewController = controllerNav
        */
        
        
    }
    
    
}
