//
//  AcceptedDateTableViewCell.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/15/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit

class AcceptedDateTableViewCell: UITableViewCell {

    @IBOutlet var status: UILabel!
    @IBOutlet var date: UILabel!
    @IBOutlet var time: UILabel!
    @IBOutlet var comments: UILabel!
    @IBOutlet var location: UILabel!
    @IBOutlet weak var theConfirmedDate: UILabel!
    @IBOutlet weak var theConfirmedLocation: UILabel!
  

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
