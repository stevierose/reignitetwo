//
//  ImageViewController.swift
//  ReIgniteThree
//
//  Created by Steven Roseman on 5/8/16.
//  Copyright © 2016 Steven Roseman. All rights reserved.
//

import UIKit
import Parse

class ImageViewController: UIViewController {
    
    @IBOutlet var imageView: UIImageView!
    var message:PFObject!
    var session:NSTimer!

    override func viewDidLoad() {
        super.viewDidLoad()

        timer()
        let imageFile:PFFile = message.objectForKey("imageFile") as! PFFile
        let imageFileUrl:NSURL = NSURL(string: imageFile.url!)!
        let imageData:NSData = (NSData(contentsOfURL: imageFileUrl))!
        imageView.image = UIImage(data: imageData)!
        print(imageView.image)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        
        
    }
    
    func timer() {
        session = NSTimer.scheduledTimerWithTimeInterval(10.0, target: self, selector: #selector(ImageViewController.sessionTimeout), userInfo: nil, repeats: false)
    }
    
    func sessionTimeout(){
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let vc: UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
        self.presentViewController(vc, animated: true, completion: nil)
       // navigationController?.popViewControllerAnimated(true)
        
    }

   
}
